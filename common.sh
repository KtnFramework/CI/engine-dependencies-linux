#!/usr/bin/env bash

function remove_folder {
    if [ -d $1 ]; then
        rm -rf $1
    else
        echo "The given path is not a folder: $1"
    fi
}

function print_command {
    echo "Command: $1" >> $2
    echo "Output:" >> $2
    echo "$($1)" >> $2
    echo "--------------------------------------------------------------------------------" >> $2
}