#!/usr/bin/env bash
. common.sh

chrono_version="7.0.0"
archive_name="chrono_$chrono_version"
package_dir_relative="$archive_name"
package_dir="$(pwd)/$package_dir_relative"

if [ ! -d chrono ]; then
    git clone https://github.com/projectchrono/chrono.git
fi
cd chrono
git checkout "$chrono_version"
cd ..

remove_folder $package_dir && mkdir $package_dir
touch $package_dir/buildInfo
print_command "lscpu" $package_dir/buildInfo
print_command "cat /etc/*-release" $package_dir/buildInfo
print_command "cmake --version" $package_dir/buildInfo
print_command "ninja --version" $package_dir/buildInfo
print_command "g++ --version" $package_dir/buildInfo

cd chrono && remove_folder build && mkdir build && cd build
cmake -DBUILD_DEMOS=OFF -DCMAKE_INSTALL_PREFIX="$package_dir" \
-DENABLE_MODULE_COSIMULATION=ON -G Ninja ..
cmake --build . --config Release --target install
cd ../..

# the data directory is installed even if no demo is built
remove_folder "$package_dir/share"

tar -czf "${archive_name}_$(date +%s).tar.gz" $package_dir_relative